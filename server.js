var express = require('express');
var app = express();
var request = require("request");

app.use(express.static(__dirname + '/'));

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.get('/getActivity', function(req, res) {

	var options = { method: 'POST',
	  url: 'https://api.kloh.in/kloh/external/v1/activity/list',
	  headers: 
	   { 
	     	'content-type': 'application/json' 
       },
	  body: { 
	  	location: { 
	  		lat: 12.9716, 
	  		lon: 77.5946 
	  	} 
	  },
	  json: true 
	};

	request(options, function (error, response, body) {
	  if (error) throw new Error(error);
	  res.json(body.response.results);
	});
});

app.get('/showActivity', function(req, res) {
	var request = require("request");

	var options = { method: 'GET',
	  url: 'https://api.kloh.in/kloh/external/v1/activity/'+ req.query.activityId,
	  headers: 
	   { 'postman-token': 'c1f35be1-0334-0aa9-26b8-0c1d3895c9a6',
	     'cache-control': 'no-cache' } };

	request(options, function (error, response, body) {
	  if (error) throw new Error(error);

	  var dat = JSON.parse(body);
	  res.json(dat);
	});
});

app.listen(3000, function() {
    console.log('Example app listening on port 3000!');
});