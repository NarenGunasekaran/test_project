$(document).ready(function() {
	$.ajax({
		type: 'GET',
		url: '/getActivity?lat=12.9716&lon=77.5946',
		success: function(data) {
			var i = 0;
			var values = Object.values(data);
			for(key in values){
				var obj = values[key];

				$('#sectionContent').append('<section><div class="container"><div class="row align-items-center"><div class="col-md-6 order-2"><div class="p-5"><img class="img-fluid rounded-circle" src='+obj.imageUrl+' alt=""></div></div><div class="col-md-6 order-1"><div class="p-5"><h2 class="display-4"><img class="img-fluid rounded-circle" style="width:100px;height;400px;" src='+obj.ownerProfileImageUrl+' alt="">'+obj.activityType+'</h2><h5>'+obj.location.address+'</h5><h6>'+obj.activityTime.activityDateString+'</h6><button class="btn btn-primary" id="showActivity_'+i+'" value="'+obj.activityId+'">JOIN</button></div></div></div></div></section>');

				i = i +1;
			}

			for(var j=0; j<values.length; j++){
				$('#showActivity_'+j).click(function(){
				var a = $(this).attr('value');
					window.location = '/activity.html?activityId='+a;
					
				});
			}
		}
	});
});

