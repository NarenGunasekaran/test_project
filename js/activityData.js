$(document).ready(function() {
	var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;
    var activityId = '';    
    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === 'activityId') {
            activityId = sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
    

	$.ajax({
		type: 'GET',
		url: '/showActivity?activityId='+activityId,
		success: function(data) {
			var values = Object.values(data);
			console.log(values);
			for(key in values){
				var obj = values[key];
				$('#containerData').css('background', 'url('+obj.imageUrl+')');
				$('#activityTitle').append(obj.activityType);
				$('#activityTime').append(obj.activityTime.activityDateString);
				$('#activityImage').attr('src', 'https://maps.googleapis.com/maps/api/js?callback=myMap&key=AIzaSyCG2k9Y9epv4fEbJy6GRI5sKY-NTNl2EFQ');
				$('#activityTitleContent').append(obj.activityType);
				$('#activityDescritpion').append(obj.description);
				$('#activityImagehost').attr('src', obj.ownerProfileImageUrl);

				function init_map1() {
	            var myLocation = new google.maps.LatLng(obj.location.lat, obj.location.lon);
		            var mapOptions = {
		                center: myLocation,
		                zoom: 16
		            };
		            var map = new google.maps.Map(document.getElementById("map1"),
		                mapOptions);
		        }
		        init_map1();
			}
			// $('#containerData').css('background', 'url('++')');


		}
	});



});

